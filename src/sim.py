
from sim_param import g_list, J_list, times, J_6ring, psi_sse, psi_C1, psi_F0, J_matrix_list, n, times_zoom
from sim_prep import simulate
from qutip import tensor

dtdir = '../results/'

tag = 'sse-F0'
psi0 = tensor(psi_F0,psi_sse)
simulate(g_list, J_list, times, J_6ring, psi0, tag, n=n, dtdir=dtdir)

tag = 'sse-F0-J1'
simulate(g_list, J_list, times, J_matrix_list[0], psi0, tag, n=n, dtdir=dtdir)

tag = 'sse-F0-J2'
simulate(g_list, J_list, times, J_matrix_list[1], psi0, tag, n=n, dtdir=dtdir)

tag = 'sse-F1-J1'
psi0 = tensor(psi_F, psi_sse)
simulate(g_list, J_list, times, J_matrix_list[0], psi0, tag, n=n, dtdir=dtdir)

tag = 'sse-F1-J2'
simulate(g_list, J_list, times, J_matrix_list[1], psi0, tag, n=n, dtdir=dtdir)

tag = 'sse-F1'
simulate(g_list, J_list, times, J_6ring, psi0, tag, n=n, dtdir=dtdir)

tag = 'sse-C1-J1'
psi0 = tensor(psi_C1, psi_sse)
simulate(g_list, J_list, times, J_matrix_list[0], psi0, tag, n=n, dtdir=dtdir)

tag = 'sse-C1-J2'
simulate(g_list, J_list, times, J_matrix_list[1], psi0, tag, n=n, dtdir=dtdir)

tag = 'sse-C1'
simulate(g_list, J_list, times, J_6ring, psi0, tag, n=n, dtdir=dtdir)

# zoom for Fig 3 & 5

tag = 'sse-F0-J1-zoom'
psi0 = tensor(psi_F0,psi_sse)
simulate(g_list, J_list, times_zoom, J_matrix_list[0], psi0, tag, n=n, dtdir=dtdir)

tag = 'sse-C1-J2-zoom'
psi0 = tensor(psi_C1, psi_sse)
simulate(g_list, J_list, times_zoom, J_matrix_list[1], psi0, tag, n=n, dtdir=dtdir)
