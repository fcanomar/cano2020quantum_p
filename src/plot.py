#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  4 21:23:32 2022

@author: fcm
"""

import pickle
import matplotlib.pyplot as plt
from sim_param import times, times_zoom
from sim_prep import get_rpears_seq
import numpy as np

N = 6

dtdir = '/Users/fcm/Documents/Publications/_Repos/cano2020quantum_P/results/'

# Fig 2

fig_list = [['fig2-1', 'sse-F0-J1', 'g=0.1,J=0.8'],
            ['fig2-2', 'sse-F0-J1', 'g=1.0,J=0.8'],
            ['fig3',   'sse-F0-J1', 'g=10.0,J=0.8'],
            ['fig4-1', 'sse-F1-J2', 'g=1.0,J=0.1'],
            ['fig4-2', 'sse-F1-J2', 'g=1.0,J=0.8'],
            ['fig5-1',   'sse-C1-J2', 'g=10.0,J=0.1'],
            ['fig5-2',   'sse-C1-J2', 'g=10.0,J=0.8'],
            ['fig6',   'sse-C1-J1', 'g=1.0,J=0.8'],
            ['fig8',   'sse-F0', 'g=1.0,J=0.8']]
            
fig_zoom_list = ['fig3', 'fig5-1', 'fig5-2']  

for [fig_tag, case_tag, gJ_tag] in fig_list:

    file = open(dtdir + case_tag + '-results.data','rb')
    sz_dict = pickle.load(file, encoding='latin1')
    
    # refactor note: could load data from dict instead of calculating
    # file = open(dtdir + case_tag + '-rpears_seq_dict.data','rb')
    # rpears_seq_dict = pickle.load(file, encoding='latin1')
    
    sz_seq = np.asarray(sz_dict[gJ_tag].expect)
    rpears_seq = get_rpears_seq(sz_seq, times, n_slice = 5,  jump = 3)
    times_f = np.linspace(min(times), max(times),len(rpears_seq))
    
    plt.figure(figsize=(13,6))    
    
    for k in range(N): 
        #ax.plot(times, seq[J_index,g_index,:,k])
        plt.plot(times, sz_seq[k,:],label='Sz (molecule ' + str(k + 1) +')')
      
    plt.plot(times_f, rpears_seq,'--',label='C')
    
    # changing the fontsize of ticks
    plt.yticks(fontsize=17)
    plt.xticks(fontsize=17)
     
    #plt.ylabel('Sz (solid), C (dashed)')
    plt.xlabel('time (s)', fontsize=20)
    plt.title(gJ_tag.replace(',','  '),fontsize=18)
    plt.legend(bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0, borderpad=1.5,prop={"size":20})
    plt.savefig('../figures/' + fig_tag + '_' + case_tag + '_' + gJ_tag +'.png', bbox_inches='tight')
    plt.show()

# zoom 0 to 5 seconds for figures 3 and 5
 
# refactor note: could write function to avoid duplicate this code   

for [fig_tag, case_tag, gJ_tag] in fig_list:
    
    if fig_tag in fig_zoom_list:
        
        file = open(dtdir + case_tag + '-zoom-results.data','rb')
        sz_dict = pickle.load(file, encoding='latin1')
    
        sz_seq = np.asarray(sz_dict[gJ_tag].expect)
        rpears_seq = get_rpears_seq(sz_seq, times_zoom, n_slice = 5,  jump = 3)
        times_f = np.linspace(min(times_zoom), max(times_zoom),len(rpears_seq))
    
        plt.figure(figsize=(13,6))    
    
        for k in range(N): 
            #only up to 5 s
            plt.plot(times_zoom, sz_seq[k,:],label='Sz (molecule ' + str(k + 1) +')')
      
        plt.plot(times_f, rpears_seq,'--',label='C')
    
        # changing the fontsize of ticks
        plt.yticks(fontsize=17)
        plt.xticks(fontsize=17)
     
        #plt.ylabel('Sz (solid), C (dashed)')
        plt.xlabel('time (s)', fontsize=20)
        plt.title(gJ_tag.replace(',','  '),fontsize=18)
        plt.legend(bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0, borderpad=1.5,prop={"size":20})
        plt.savefig('../figures/' + fig_tag + '_' + case_tag + '_' + gJ_tag +'-zoom.png', bbox_inches='tight')
        plt.show()
        
    
    