from process_prep import get_rpears_dict
import pickle
from sim_param import times
import numpy as np

dtdir='../results/'

tag_list = ['sse-C1',
 'sse-F1-J2',
 'sse-C1-J2',
 'sse-F0-J2',
 'sse-C1-J1',
 'sse-F0-J1',
 'sse-F0',
 'sse-F1',
 'sse-F1-J1']

tag_list = ['sse-C1',
 'sse-F1-J2',
 'sse-C1-J2',
 'sse-F0-J2',
 #'sse-C1-J1',
 'sse-F0-J1',
 'sse-F0',
 'sse-F1',
 'sse-F1-J1']

for tag in tag_list:
    print(tag)
    get_rpears_dict(tag, dtdir=dtdir)
    
for tag in tag_list:
    print(tag)
    with open(dtdir + tag + '-results.data', 'rb') as filehandle:
        result_dict = pickle.load(filehandle)
        rpears_seq_dict = {tag:None for tag in tag_list} 
        for key in result_dict.keys():
            rpears_seq_dict[key] = get_rpears_seq(result_dict[key].expect,times)
    with open(dtdir + tag + '-rpears_seq_dict.data', 'wb') as filehandle:
        pickle.dump(rpears_seq_dict, filehandle) 