
import pickle
from qutip import basis, tensor, sigmap, qeye, destroy, mesolve, sigmaz
import numpy as np
from scipy.stats.stats import pearsonr

def S_singlex(N,i):
    S = [basis(2,1) for j in range(N)]
    S[i] = basis(2,0)
    return tensor(S)

def Sp_singlex_i(N,i):
    op_list = [qeye(2) for j in range(N)]
    op_list[i] = sigmap()
    Sp = tensor(op_list)
    return Sp

def Sp_singlex(N):
    Sp = sum([Sp_singlex_i(N,i) for i in range(N)])
    return Sp

def Sm_singlex_i(N,i):
    Sm = Sp_singlex_i(N,i).dag()
    return Sm

def Sm_singlex(N):
    Sm = Sp_singlex(N).dag()
    return Sm

def Sz_singlex_i(N,i):
    op_list = [qeye(2) for j in range(N)]
    op_list[i] = sigmaz()
    Sz = tensor(op_list)
    return Sz

def Sz_singlex(N):
    Sz = sum([Sz_singlex_i(N,i) for i in range(N)])
    return Sz

def S(N,i,nu):
    return basis(2 * N, nu * N + i - 1)

def S_(N,i,nu):
    return S(N,i+1,nu)

def Hm_singlex(N, E=0.44):
     Hm = 0
     for i in range(N):
         Hm += E * S_singlex(N,i) * S_singlex(N,i).dag()
     return Hm

# rename?! <2021-11-05 Fri> -> substitute by Hmi_Jmatrix <2021-11-05 Fri>
def Hmi_6ring_Jmatrix(J):
    N = 6
    Hmi = 0
    for i in range(6):
        for j in range(6):
            if i != j:
                Hmi += J[i][j] * S_singlex(N,i) * S_singlex(N,j).dag()
    return Hmi

def Hi_singlex(N, g = 1.0, n=None, es_ordering=False):
    """N: number of molecules, n: max number of photons in the truncated Fock space"""
    if n==None:
        a = destroy(N + 1)
    else:
        a = destroy(n + 1)
    if es_ordering:
        Hi = g * (tensor(a,Sp_singlex(N)) + tensor(a.dag(),Sm_singlex(N)))
    else:
        Hi = g * (tensor(Sp_singlex(N),a) + tensor(Sm_singlex(N),a.dag()))
    return Hi

def Hmi_Jmatrix(J):
    assert(J.shape[0] == J.shape[1])
    N = J.shape[0]
    Hmi = 0
    for i in range(N):
        for j in range(N):
            if i != j: # assured in theory by Jii = 0 anyway <2021-11-05 Fri>
                Hmi += J[i][j] * S_singlex(N,i) * S_singlex(N,j).dag()
    return Hmi

def Hph(E=0.44, n=1):
    '''
    Photon Hamiltonian
    truncated to n photons
    '''
    a = destroy(n + 1)
    H = E * a.dag() * a
    return H

def Hphc(N, E=0.44, n=1):
    '''
    Photon Hamiltonian in the combined space (E,S) 
    for N molecules 
    and truncated to n photons
    '''
    Imol = tensor([qeye(2) for i in range(N)])
    H = tensor(Hph(E,n),Imol)
    return H

def sz_list_OLDER(N):
    sz_list = []
    for i in range(N):
        sz_list.append(.5 * (S_(N,i,1) * S_(N,i,1).dag() - S_(N,i,0) * S_(N,i,0).dag()))
    return sz_list

def sz_list(N):
    op_list = []
    aux = [qeye(2) for i in range(N)]
    for i in range(N):
        aux[i] = sigmaz()
        op = tensor(aux)
        op_list.append(op)
    return op_list

def simulate(g_list, J_list, times, J_matrix, psi0 = None, tag = 'sim', N = 6, E=0.44, c_ops=[], dtdir = './', n=None, write_file=True):
    if n == None: n=N 
    if (psi0 == None): psi0 = tensor(basis(n+1,0), tensor([basis(2,1) for i in range(N)]))
    result_dict = dict()
    op = sz_list(N) # rf: move elsewhere? 
    opc = [tensor(basis(n+1,0),op[i]) for i in range(N)] # rf: move elsewhere?
    for g in g_list:
        for J in J_list:
            param_tag = 'g=' + str(round(g,2)) + ',J=' + str(round(J,2))
            H_molc = tensor(qeye(n+1), Hm_singlex(N=N,E=E) + Hmi_Jmatrix(J * J_matrix))
            H_phc = Hphc(N=N,n=n)
            H_int = Hi_singlex(N=N,g=g,n=n,es_ordering=True)
            H = H_molc + H_phc + H_int
            result = mesolve(H, psi0, times, [], opc)
            result_dict[param_tag] = result
    if write_file==True:
        with open(dtdir + tag + '-results.data', 'wb') as filehandle:
            pickle.dump(result_dict, filehandle)
    else:
        return result_dict

def rpears_window(result):
    for i in range(len(result.states)):
        r = ptrace(rho,range(0,6))
        s_list = []
        for i in range(6):
            a = range(6)
            a.remove(i)
            s_i = entropy_mutual(r,i,a)
        s_list.append(s_i)
    s = sum(s_list)/6    
    return s

def get_rpears_seq(sz_seq, times, n_slice = 5,  jump = 3):
  rpears_seq = []
  for i in range(len(times)//jump):
      k = jump * i 
      #print(str(k) + ' ' + str(k + n_slice)) 
      sz_slice = sz_seq[:,k:k + n_slice]
      rpears = get_pearson_mean_from_sz_seq(sz_slice)
      rpears_seq.append(rpears)
  return rpears_seq

def get_pearson_mean_from_sz_seq(sz_seq):
    #N = get_N(result)
    N = 6
    pr = get_pearson_matrix_from_sz_seq(sz_seq)
    mr = sum(sum(abs(pr)))/float(N * (N - 1))
    return mr

def get_pearson_matrix_from_sz_seq(sz_seq):
    #N = get_N(result)
    N = 6
    M = np.zeros([N,N])
    for i in range(N):
        for j in range(N):
            if i < j:
                x = sz_seq[i,:]
                y = sz_seq[j,:]
                r = pearsonr(x, y)[0]
                M[i,j] = M[j,i] = r
    return M

def get_psi_sse(N):
    se_list = []
    for i in range(N):
        se = [basis(2,1) for j in range(N)]
        se[i] = basis(2,0)
        se_list.append(tensor(se))
    psi_sse = sum(se_list)
    return psi_sse
