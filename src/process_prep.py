
from sim_param import N, g_list, J_list
from qutip import qeye, sigmaz, tensor
import pickle
import numpy as np
from scipy import stats
from os import listdir
import re
from scipy.stats.stats import pearsonr

sz_op_list = []
for i in range(N):
  op = [qeye(2) for j in range(N)]
  op[i] = sigmaz()
  sz = tensor(op)
  sz_op_list.append(sz)

def get_pearson_matrix_from_sz_seq(sz_seq):
    M = np.zeros([N,N])
    for i in range(N):
        for j in range(N):
            if i < j:
                x = sz_seq[:,i]
                y = sz_seq[:,j]
                r = pearsonr(x, y)[0]
                M[i,j] = M[j,i] = r
    return M

def get_pearson_mean_from_sz_seq(sz_seq):
    pr = get_pearson_matrix_from_sz_seq(sz_seq)
    mr = sum(abs(pr))/float(N * (N - 1))
    return mr

def write_sz_seq(tag_list,dtdir='./'):
    sz_dict = dict(zip(tag_list, [None]*len(tag_list)))
    for tag in tag_list:
        with open(dtdir + tag + '-results.data', 'rb') as filehandle:
            result_matrix = pickle.load(filehandle)
            sz_matrix = []
            for i_g in range(len(g_list)):
                sz_matrix_ = []
                for i_J in range(len(J_list)):
                    sz_seq = []
                    result = result_matrix[i_g][i_J]
                    for t in range(len(times)):
                        state = result.states[t]
                        sz = [state.dag() * tensor(sz_op_list[i],qeye(N+1)) * state for i in range(N)]
                        sz_seq.append([real(sz[i].data.data[0]) for i in range(N)])
                    sz_matrix_.append(sz_seq)
                sz_matrix.append(sz_matrix_) 
        sz_dict[tag] = sz_matrix
        file_pi = open(dtdir + tag + '-sz_dict.obj', 'w') 
        pickle.dump(sz_dict, file_pi)

# now results has sigmaz expectation values 
def get_N(result):
    assert len(result.expect) != 0, "no results!"
    N = len(result.expect)-1
    return N
    
# now results has sigmaz expectation values   
def get_pearson_matrix(result):
    N = get_N(result)
    M = np.zeros([N,N])
    for i in range(N):
        for j in range(N):
            if i < j:
                x = np.real(result.expect[i])
                y = np.real(result.expect[j])
                r = stats.pearsonr(x, y)[0]
                M[i,j] = M[j,i] = r
    return M

def get_pearson_mean(result):
    N = get_N(result)
    pr = get_pearson_matrix(result)
    mr = sum(abs(pr))/float(N * (N - 1))
    return mr  

def get_tag_list(dir='../results/'):
    filelist = listdir(dir)
    r = re.compile(".*-results.data")
    results_filelist = list(filter(r.match, filelist)) # Read Note below
    tag_list = [item.replace('-results.data','') for item in results_filelist]
    return tag_list
        
def get_rpears_dict(tag, dtdir='./'):
    '''
    Returns nested dictionary with keys indicating: initial state,
    environment, etc.

    Values are dictionaries for each (g,J) parameters case
    '''
    # get g_list and J_list, or maybe use dataframes?
    with open(dtdir + tag + '-results.data', 'rb') as filehandle:
        result_dict = pickle.load(filehandle, encoding="latin1")
    rpears_dict = dict()
    for g in g_list:
        for J in J_list:
            param_tag = 'g=' + str(round(g,2)) + ',J=' + str(round(J,2))
            result = result_dict[param_tag]
            pearson_mean = get_pearson_mean(result)
            rpears_dict[param_tag] = pearson_mean 
    file_pi = open(dtdir + tag + '-rpears_mean.obj', 'wb')
    pickle.dump(rpears_dict, file_pi)

def get_pearson_matrix_from_sz_seq(sz_seq):
    #N = get_N(result)
    N = 6
    M = np.zeros([N,N])
    for i in range(N):
        for j in range(N):
            if i < j:
                x = sz_seq[i,:]
                y = sz_seq[j,:]
                r = pearsonr(x, y)[0]
                M[i,j] = M[j,i] = r
    return M

def get_rpears_seq(sz_seq, times, n_slice = 5,  jump = 3):
    '''
    R Pearson calculation with a time window from sz sequences
    '''
    rpears_seq = []
    sz_seq = np.array(sz_seq)
    for i in range(len(times)//jump):
        k = jump * i 
        #print(str(k) + ' ' + str(k + n_slice)) 
        sz_slice = sz_seq[:,k:k + n_slice]
        rpears = get_pearson_mean_from_sz_seq(sz_slice)
        rpears_seq.append(rpears)
    return rpears_seq