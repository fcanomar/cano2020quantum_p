
import numpy as np
import random
from qutip import *
random.seed(101)
N = 6
n = 6
J = 0.8
E = 0.44
J_6ring = np.array([[0,1,0,0,0,1],[1,0,1,0,0,0],[0,1,0,1,0,0],[0,0,1,0,1,0],[0,0,0,1,0,1],[1,0,0,0,1,0]])
J_3ring = np.array([[0,1,1],[1,0,1],[1,1,0]])
J_matrix_list = []
np.random.seed(0)
for i in range(10):
    b = J * np.random.random_sample(size=(N,N))
    J_symm = (b + b.T)/2
    J_symm[range(N), range(N)] = 0
    J_matrix_list.append(J_symm)

psi_g = tensor([basis(2,1),basis(2,1), basis(2,1), basis(2,1), basis(2,1), basis(2,1)])
psi_e = tensor([basis(2,0),basis(2,1), basis(2,1), basis(2,1), basis(2,1), basis(2,1)])
psi_F1 = fock(n+1,1)
psi_F0 = fock(n+1,0)
psi_C1 = coherent(n+1,1)

se_list = []
for i in range(N):
    se = [basis(2,1) for j in range(N)]
    se[i] = basis(2,0)
    se_list.append(tensor(se))

psi_sse = sum(se_list)

times = np.linspace(1,30.0,500)
times_zoom = np.linspace(1,5.0,300)
Jmin = 0.0
Jmax = 0.8
n_J = 9
J_list = np.linspace(Jmin, Jmax, n_J)
g_list = np.asarray([0.0,0.01,0.1,1.0,10.0])
n_g = len(g_list)
